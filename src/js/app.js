"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import tabs from './components/tabs.js'; // Tabs
import quantity from './forms/quantity.js' // input number
import Rating from './components/rating.js'; // Rating plugin
import Order from './components/item-order.js' // input number
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// input Number
quantity()

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();

// Rating
Rating();

// Product order
Order();

/*
Документация плагина: https://refreshless.com/nouislider/ */
import "./forms/range.js";

// Sliders
import "./components/sliders.js";

/* Настройка цветов */
import "./components/settings.js";
