// Подключение из node_modules
import * as noUiSlider from 'nouislider';

export function rangeInit() {

    const dataSliders = document.querySelectorAll('.data-slider');
    if (dataSliders.length > 0) {
        dataSliders.forEach(el => {
            let sliderStart = el.getAttribute('data-start').split(",").map(parseFloat);
            let sliderMin = parseFloat(el.getAttribute('data-min'));
            let sliderMax = parseFloat(el.getAttribute('data-max'));
            noUiSlider.create(el, {
                start: sliderStart,
                range: {
                    'min': sliderMin,
                    'max': sliderMax,
                }
            });
        });
    }

    const rangeSliders = document.querySelectorAll('[data-range]');
    if (rangeSliders.length > 0) {
        rangeSliders.forEach(el => {
            const range = el
            const rangeSlider = range.querySelector('[data-range-slider]')
            const rangeFrom = range.querySelector('[data-range-field="from"]')
            const rangeTo = range.querySelector('[data-range-field="to"]')

            const rangeStart = rangeSlider.getAttribute('data-start').split(",").map(parseFloat);
            const rangeMin = parseFloat(rangeSlider.getAttribute('data-min'));
            const rangeMax = parseFloat(rangeSlider.getAttribute('data-max'));


            noUiSlider.create(rangeSlider, {
                start: rangeStart,
                connect: true,
                step: 1,
                range: {
                    'min': rangeMin,
                    'max': rangeMax,
                }
            });

            rangeSlider.noUiSlider.on('update', function (values, handle) {
                if (handle) {
                    rangeTo.value = Math.ceil(values[handle])
                } else {
                    rangeFrom.value = Math.ceil(values[handle])
                }
            });

            rangeFrom.addEventListener('change', function () {
                rangeSlider.noUiSlider.set([this.value, null]);
            });

            rangeTo.addEventListener('change', function () {
                rangeSlider.noUiSlider.set([null, this.value]);
            });
        });
    }

}
rangeInit();
