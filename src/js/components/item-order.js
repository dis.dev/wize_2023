export default () => {

    if(document.querySelector('[data-product]')) {
        const product           = document.querySelector('[data-product]')
        const productTerm       = product.querySelector('[data-product-term]')
        const productMileage    = product.querySelector('[data-product-mileage]')
        const productQuantity   = product.querySelector('[data-product-quantity]')
        const productPrice      = product.querySelector('[data-product-price]')
        const productCounter    = product.querySelector('[data-product-counter]')
        const productCost       = product.querySelector('[data-product-price]')


        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-term]')) {
                productTerm.dataset.productTerm = parseInt(event.target.closest('[data-term]').value)

                summary()
            }

            if (event.target.closest('[data-mileage]')) {
                productMileage.dataset.productMileage = parseInt(event.target.closest('[data-mileage]').value)

                summary()
            }

            if (event.target.closest('[data-quantity-button]')) {
                productQuantity.dataset.productQuantity = parseInt(event.target.closest('[data-quantity]').querySelector('[data-quantity-input]').value)

                summary()
            }
        })

        product.querySelector('[data-amount]').addEventListener('change', function (){
            productQuantity.dataset.productQuantity = parseInt(this.value)

            summary()
        })

        function summary() {
            const term = parseInt(productTerm.dataset.productTerm)
            const mileage = parseInt(productMileage.dataset.productMileage)
            const quantity = parseInt(productMileage.dataset.productQuantity)
            let increase = 0
            let price = parseInt(productPrice.dataset.productPrice)



            switch (mileage) {
                case 15000:
                    break;
                case 30000:
                    increase = 10
                    break;
                case 45000:
                    increase = 15
                    break;
                default:
                    increase = 0
            }

            price = (price * term) + price * term / 100 * increase


            let prettyPrice = Math.round(price * quantity * 100) / 100

            product.querySelector('[data-count]').innerHTML = quantity
       //     product.querySelector('[data-price]').innerHTML = price
            product.querySelector('[data-summary]').innerHTML = prettyPrice

            const productSummary    = product.querySelector('[data-summary]')
            const productAmount     = product.querySelector('[data-amount]')
        }
    }
};
